import Armor from '@/data/items/gear/Armor';
import Helmet from '@/data/items/gear/Helmet';
import Weapon from '@/data/items/gear/Weapon';

class GearItem {
  data = {}
  constructor(type = '', data: any) {
    if (type) {
      switch (type) {
        case 'weapon':
          this.data = new Weapon(data)
          break
        case 'helmet':
          this.data = new Helmet(data)
          break
        case 'armor':
          this.data = new Armor(data)
          break
      }
    }
  }
}

export default {
  GearItem
}