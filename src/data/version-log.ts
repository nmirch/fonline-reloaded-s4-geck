export default [
  {
    version: 1.1,
    date: 'Jan 29, 2022',
    items: [
      'App updates are now handled in the Info Panel',
      'Reset Build button is now located in Control Panel',
      'Added Community links in InfoPanel'
    ]
  }
]