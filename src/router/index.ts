import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/desktop/Home.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/level',
    name: 'Level',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/desktop/Level.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
