import { createApp } from 'vue'
// import Clock from './plugins/clock.js';
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
const options = { s: 14 }
createApp(App).use(store).use(router).mount('#app')
// createApp(App).use(store).use(Clock, options).use(router).mount('#app')
