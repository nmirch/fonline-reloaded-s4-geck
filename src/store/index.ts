import { createStore } from 'vuex'
import newBuild from '@/data/default-build'
import methods from '@/data/methods'
import infoPanelData from '@/data/info-build'
import getThresholdData from '@/utils/getSkillThreshold'
import LS from '@/utils/localStorageHandler'
import { Inventory } from '@/data/items/gear/Inventory'
import versionLog from '@/data/version-log'

export default createStore({
  state: {
    build: LS.getLastBuild() || JSON.parse(JSON.stringify(newBuild)),
    infoPanel: {
      name: 'Info Panel',
      type: 'Default',
      description: 'Click on things to see more info about them here...',
      wiki: 'https://www.fonline-reloaded.net/wiki/Main_Page'
    },
    ui: {
      showPerks: false,
      showSupportPerks: false,
      showCharSubPanel: false,
      activePopup: 'BuildInfoPopup',
      progressBarStyle: {
        activeStyle: 'default',
        default: {
          active: true,
          next: 'currentAndMax'
        },
        currentAndMax: {
          active: false,
          next: 'percent'
        },
        percent: {
          active: false,
          next: 'none'
        },
        none: {
          active: false,
          next: 'default'
        },
      },
    },
    system: {
      minifyPanels: {
        info: false,
        skills: false,
        charStats: false
      },
      screen: {
        innerWidth: 0,
        innerHeight: 0
      },
      versionLog: versionLog
    }
  },
  // ------------------------------------------------------------------------------
  // -----------------------------M-U-T-A-T-I-O-N-S--------------------------------
  // ------------------------------------------------------------------------------
  mutations: {
    // ---------------------------
    // ----------SYSTEM-----------
    // ---------------------------
    saveScreenData(state, data) {
      state.system.screen.innerWidth = data.innerWidth
      state.system.screen.innerHeight = data.innerHeight
      if (data.innerWidth < 380) {
        // console.log('data.innerWidth < 380', data.innerWidth)
      } else if (data.innerWidth < 450) {
        // console.log('data.innerWidth < 450', data.innerWidth)
      } else if (data.innerWidth < 600) {
        // console.log('data.innerWidth < 600', data.innerWidth)
      } else {
        // console.log('data.innerWidth >= 600', data.innerWidth)
      }
    },
    // ---------------------------
    // -----------GAME------------
    // ---------------------------
    resetBuild(state) {
      state.build = JSON.parse(JSON.stringify(newBuild))
    },
    loadBuild(state, build) {
      state.build = build
    },
    createChar(state) {
      const perkStep = state.build.Traits.Skilled.tagged ? 4 : 3
      state.build.buildInfo.phase = 'level'
      state.build.buildInfo.currentLevel = 1
      state.build.buildInfo.currentPerk.step = perkStep
      state.build.buildInfo.currentPerk.nextAtLevel = perkStep

      Object.keys(state.build.Skills).forEach(skill => {
        if (state.build.Skills[skill].tagged) {
          state.build.buildInfo.Skills.push(skill)
        }
      })
      Object.keys(state.build.Traits).forEach(trait => {
        if (state.build.Traits[trait].tagged) {
          state.build.buildInfo.Traits.push(state.build.Traits[trait])
        }
      })
    },
    mutateSpecial(state, data) {
      const { statName, statValue, pool } = data
      state.build.SPECIAL[statName].baseValue = statValue
      state.build.SPECIAL.PL.baseValue = pool
    },
    selectSkill(state, skillName) {
      Object.keys(state.build.Skills).forEach(skill => {
        state.build.Skills[skill].selected = false
      })
      state.build.Skills[skillName].selected = true
    },
    modifySkill(state, data) {
      const { skill, modifier, stepSP, availableSP, change, spentSP } = data
      if (modifier === 'book') {
        state.build.Books[skill].gainedSkillValue = data.bookSkillValue
        state.build.Books[skill].read = data.booksRead
        state.build.Books[skill].available = false
      } else if (modifier === 'boost') {
        Object.keys(state.build.Boosts).forEach(boost => {
          state.build.Boosts[boost].available = false
          if (boost === skill) {
            state.build.Boosts[boost].tagged = true
            state.build.buildInfo.marshalBoost = true
          }
        })
      } else {
        state.build.Skills[skill].stepSkillValue += stepSP
        state.build.Skills[skill].change = change
        state.build.buildInfo.availableSkillPoints = availableSP
        if (modifier === '+') {
          state.build.Skills[skill].spentSP += spentSP
        } else if (modifier === '-') {
          state.build.Skills[skill].spentSP = spentSP
        }
      }
    },
    addStepSkillValues(state) {
      Object.keys(state.build.Skills)
        .forEach(skill => {
          state.build.Skills[skill].gainedSkillValue += state.build.Skills[skill].stepSkillValue
          state.build.Skills[skill].stepSkillValue = 0
        })
    },
    handleLevel(state, data) {
      const { multiplier } = data
      state.build.buildInfo.currentLevel += multiplier
      state.build.buildInfo.availableSkillPoints += multiplier * methods.CharStats.SkillPointsPerLevel.updateStat(state.build)
    },
    handleAvailablePerk(state, data) {
      const { available, tagged, nextAtLevel } = data
      state.build.buildInfo.currentPerk.available = available
      state.build.buildInfo.currentPerk.tagged = tagged
      state.build.buildInfo.currentPerk.nextAtLevel += nextAtLevel
      if (available) {
        state.ui.showPerks = true
      }
    },
    tagPerk(state, data) {
      const { category, perk } = data
      state.build[category][perk].tagged = true
      state.build[category][perk].available = false
      state.build[category][perk].taggedAt = state.build.buildInfo.currentLevel
      if (category === 'Perks') {
        state.build.buildInfo.currentPerk.available = false
        state.build.buildInfo.currentPerk.tagged = true
        // state.ui.showPerks = false
      }
    },
    toggleDrug(state, data) {
      const { drug } = data
      state.build.Drugs[drug].tagged = !state.build.Drugs[drug].tagged
    },
    changeProgressBarStyle(state) {
      let allowMutation = true
      Object.keys(state.ui.progressBarStyle).forEach(setting => {
        if (state.ui.progressBarStyle[setting].active && allowMutation && setting !== 'activeStyle') {
          state.ui.progressBarStyle[setting].active = false
          const next = state.ui.progressBarStyle[setting].next
          state.ui.progressBarStyle[next].active = true
          state.ui.progressBarStyle.activeStyle = setting
          allowMutation = false
        }
      })
    },
    implantSPECIAL(state, data) {
      const { stat } = data
      state.build.SPECIAL[stat].implanted = true
    },
    implantCombat(state, data) {
      const { implant } = data
      state.build.CombatImplants[implant].tagged = true
    },
    takeProfession(state, data) {
      const { profession } = data
      state.build.Professions[profession].tagged = true
    },
    saveBuild(state, data) {
      const saveData = {
        ...data,
        build: { ...state.build }
      }
      // call LS function - saveBuildLS(saveData)
      LS.saveBuildLS(saveData)
    },
    resetCurrentBuildLS() {
      LS.resetCurrentBuild()
    },
    handleHitPoints(state, data) {
      const { multiplier } = data
      const diff = state.build.buildInfo.currentPerk.nextAtLevel - state.build.buildInfo.currentLevel
      if (multiplier === -1 && diff > 1) {
        state.build.CharStats.HitPoints.gainedValue += diff * (state.build.SPECIAL.EN.baseValue / 2)
      } else {
        state.build.CharStats.HitPoints.gainedValue += state.build.SPECIAL.EN.baseValue / 2
      }
    },
    updateBuildDetails(state, data) {
      const { field, value } = data
      state.build.buildInfo[field] = value
    },
    equipItem(state, data) {
      const { slot, key } = data
      state.build.EquippedGear[slot] = Inventory[slot][key]
    },
    equipDefaultGear(state) {
      if (!state.build.EquippedGear.head && !state.build.EquippedGear.body) {
        state.build.EquippedGear.head = Inventory.head.NoHelmet
        state.build.EquippedGear.body = Inventory.body.VaultSuit
      }
    }
  },
  // ------------------------------------------------------------------------------
  // --------------------------------A-C-T-I-O-N-S---------------------------------
  // ------------------------------------------------------------------------------
  actions: {
    // ---------------------------
    // ----------SYSTEM-----------
    // ---------------------------
    handleScreenData({ commit }, data) {
      commit('saveScreenData', data)
    },
    // ---------------------------
    // -----------GAME------------
    // ---------------------------
    resetBuild({ commit, dispatch }) {
      commit('resetBuild')
      commit('resetCurrentBuildLS')
      commit('equipDefaultGear')
      dispatch('refreshStats')
    },
    loadBuild({ commit, dispatch }, build) {
      commit('loadBuild', build)
      dispatch('refreshStats')
    },
    createChar({ commit, dispatch }) {
      // save build in LS/currentBuild.static
      commit('saveBuild', { type: 'static', slot: 0 })
      // create and move char into phase 'level'
      commit('createChar')
      // save build in LS/currentBuild.static
      commit('saveBuild', { type: 'static', slot: 1 })
      // save build in LS/currentBuild.dynamic
      commit('saveBuild', { type: 'dynamic', slot: 0 })
      // refresh stats
      dispatch('refreshStats')
    },
    levelUp({ state, commit, dispatch }, data) {
      // handle stepSkillValues
      commit('addStepSkillValues')
      // handle Hit-Points
      if (state.build.buildInfo.currentLevel < 24) {
        commit('handleHitPoints', data)
      }
      // handle Level & Skill Points
      dispatch('handleLevel', data)
      // handle available Perk
      dispatch('handleAvailablePerk')
      // save build in LS/currentBuild.dynamic
      commit('saveBuild', { type: 'dynamic', slot: 0 })
      // save level 24 build in LS/currentBuild.static
      if (state.build.buildInfo.currentLevel === 24) {
        commit('saveBuild', { type: 'static', slot: 2 })
      }
      // refresh stats
      dispatch('refreshStats')
    },
    refreshStats({ state }) {
      // const start = window.performance.now()
      Object.keys(state.build).forEach(category => {
        if (['SPECIAL', 'CharStats', 'Skills'].includes(category)) {
          Object.keys(state.build[category]).forEach(stat => {
            state.build[category][stat].value = methods[category][stat].updateStat(state.build)
            if (category === 'SPECIAL' && stat !== 'PL') {
              state.build.SPECIAL[stat].onDrugs = methods.SPECIAL[stat].onDrugs(state.build)
            }
          })
        }
        if (['Perks', 'SupportPerks', 'Boosts', 'CombatImplants', 'Professions'].includes(category)) {
          Object.keys(state.build[category]).forEach(stat => {
            state.build[category][stat].available = methods[category][stat].updateStat(state.build)
          })
        }
        if (category === 'AdvancedDR') {
          Object.keys(state.build.AdvancedDR).forEach(bodySlot => {
            Object.keys(state.build.AdvancedDR[bodySlot]).forEach(stat => {
              state.build.AdvancedDR[bodySlot][stat] = methods.AdvancedDR[bodySlot][stat](state.build)
            })
          })
        }
      })
      LS.saveLastBuild(state.build)
      // const end = window.performance.now()
      // console.log(`Stats refreshed in ${(end - start)} milliseconds`)
    },
    updateInfoPanel({ state }, { category, stat }) {
      state.infoPanel = { ...infoPanelData[category][stat] }
    },
    modifySpecial({ state, commit, dispatch }, { statName, modifier }) {
      let statValue = state.build.SPECIAL[statName].baseValue;
      let pool = state.build.SPECIAL.PL.baseValue;

      if (modifier === '+') {
        if (pool <= 0) {
          return;
        }
        if (statName === 'IN' && statValue === 9 && state.build.Traits.Bonehead.tagged) {
          return;
        }
        if (statValue < 10) {
          statValue += 1;
          pool -= 1;
        }
      } else if (modifier === '-') {
        if (statValue <= 1) {
          return;
        }
        if (statValue === 5 && state.build.Traits.Bruiser.tagged) {
          return;
        }
        statValue -= 1;
        pool += 1;
      }
      const mutationData = {
        statName,
        statValue,
        pool
      }
      commit('mutateSpecial', mutationData)
      dispatch("refreshStats")
    },
    tagSkill({ state, dispatch }, skill) {
      if (skill === 'Scavenging') {
        return
      }
      const tagged = state.build.Skills[skill].tagged
      const taggedSkills = state.build.buildInfo.taggedSkills
      if (tagged) {
        state.build.Skills[skill].tagged = false
        state.build.buildInfo.taggedSkills -= 1
      } else if (!tagged && taggedSkills < 3) {
        state.build.Skills[skill].tagged = true
        state.build.buildInfo.taggedSkills += 1
      } else {
        return
      }
      dispatch('refreshStats')
    },
    tagTrait({ state, dispatch }, trait) {
      const tagged = state.build.Traits[trait].tagged
      const taggedTraits = state.build.buildInfo.taggedTraits
      if (trait === 'Bruiser') {
        const ST = state.build.SPECIAL.ST.baseValue
        if (tagged) {
          if (ST < 5) {
            return
          } else {
            state.build.Traits[trait].tagged = false
            state.build.buildInfo.taggedTraits -= 1
            state.build.SPECIAL.ST.baseValue -= 4
          }
        } else if (taggedTraits < 2) {
          state.build.Traits[trait].tagged = true
          state.build.buildInfo.taggedTraits += 1
          if (ST <= 6) {
            state.build.SPECIAL.ST.baseValue += 4
          } else {
            state.build.SPECIAL.ST.baseValue += 10 - ST
            state.build.SPECIAL.PL.baseValue += 4 - (10 - ST)
          }
        }
      } else if (trait === 'Bonehead') {
        const IN = state.build.SPECIAL.IN.baseValue
        if (tagged) {
          state.build.Traits[trait].tagged = false
          state.build.buildInfo.taggedTraits -= 1
          state.build.SPECIAL.IN.baseValue += 1
        } else if (taggedTraits < 2 && IN > 1) {
          state.build.Traits[trait].tagged = true
          state.build.buildInfo.taggedTraits += 1
          state.build.SPECIAL.IN.baseValue -= 1
        }
      } else {
        if (tagged) {
          state.build.Traits[trait].tagged = false
          state.build.buildInfo.taggedTraits -= 1
        } else if (taggedTraits < 2) {
          state.build.Traits[trait].tagged = true
          state.build.buildInfo.taggedTraits += 1
        }
      }
      dispatch('refreshStats')
    },
    tagPerk({ state, dispatch, commit }, data) {
      const perk = state.build[data.category][data.perk]
      if (data.category === 'Perks') {
        const allowed = state.build.buildInfo.currentPerk
        if (perk.available && !perk.tagged && allowed.available && !allowed.tagged) {
          commit('addStepSkillValues')
          commit('tagPerk', data)
          dispatch('refreshStats')
        }
      } else if (perk.available && !perk.tagged && data.category === 'SupportPerks') {
        commit('addStepSkillValues')
        commit('tagPerk', data)
        dispatch('refreshStats')
      }

    },
    selectSkill({ commit }, skill) {
      if (skill !== 'Scavenging') {
        commit('selectSkill', skill)
      }
    },
    modifySkill({ state, dispatch, commit }, data) {
      const { skill, modifier, multiplier } = data
      const tagged = state.build.Skills[skill].tagged
      const maxSkillValue = state.build.Skills[skill].maxValue
      let spentSkillPoints = state.build.Skills[skill].spentSP
      let tempSkillValue = state.build.Skills[skill].value
      let unspentSP = state.build.buildInfo.availableSkillPoints
      let stepSkillValue = state.build.Skills[skill].stepSkillValue
      let change = state.build.Skills[skill].change
      // console.log('stepSkillValue = ', stepSkillValue)
      // console.log('tempSkillValue = ', tempSkillValue)
      // console.log('maxSkillValue = ', maxSkillValue)
      // console.log('unspentSP = ', unspentSP)
      // console.log('multiplier = ', multiplier)
      // console.log('tagged = ', tagged)
      const mutationData = {
        skill,
        modifier,
        stepSP: 0,
        availableSP: 0,
        change: 0,
        spentSP: '',
        bookSkillValue: 0,
        booksRead: 0
      }
      let updateData = false
      const valueIncrement = tagged ? 2 : 1;
      for (let i = 0; i < multiplier; i++) {
        const skillCost = getThresholdData(tempSkillValue)
        if (modifier === '+') {
          let stepSP = 0
          if (unspentSP >= skillCost.cost && tempSkillValue < maxSkillValue) {
            tempSkillValue += valueIncrement;
            stepSP += valueIncrement;
            unspentSP -= skillCost.cost;

            mutationData.spentSP += `${skillCost.cost}`;
            mutationData.stepSP += stepSP
            mutationData.availableSP = unspentSP
            if (tempSkillValue > maxSkillValue) {
              mutationData.change = tempSkillValue - maxSkillValue;
            }
            updateData = true;
          }
        } else if (modifier === '-') {
          let stepSP = 0
          if (stepSkillValue > 0) {
            const lastCost = Number(spentSkillPoints.slice(-1))
            if (change) {
              tempSkillValue -= 1
              change = 0
              mutationData.change = 0
            } else {
              tempSkillValue -= valueIncrement
            }
            stepSP -= valueIncrement;
            stepSkillValue -= valueIncrement;
            unspentSP += lastCost;
            spentSkillPoints = spentSkillPoints.slice(0, -1)
            mutationData.spentSP = spentSkillPoints
            mutationData.stepSP += stepSP
            mutationData.availableSP = unspentSP
            updateData = true;
          }
        } else if (modifier === 'book') {
          if (tempSkillValue < maxSkillValue) {
            const bookValue = Math.floor(6 / skillCost.cost)
            if (bookValue === 1) {
              tempSkillValue += tagged ? bookValue * 2 : bookValue
              mutationData.bookSkillValue += tagged ? bookValue * 2 : bookValue
              mutationData.booksRead += 1
              updateData = true
            } else {
              let bookPoints = 6
              for (let i = 0; i < 6; i++) {
                const { cost } = getThresholdData(tempSkillValue)
                const increment = tagged ? 2 : 1
                if (bookPoints < cost || tempSkillValue >= maxSkillValue) {
                  break;
                } else {
                  bookPoints -= cost
                  tempSkillValue += increment
                  mutationData.bookSkillValue += increment
                  updateData = true
                }
              }
              if (updateData) {
                mutationData.booksRead += 1
              }
            }
          }
        } else if (modifier === 'boost' && !state.build.buildInfo.marshalBoost) {
          updateData = true
          dispatch('addStepSkillValues')
        }
      }
      if (updateData) {
        updateData = false;
        // console.log(mutationData)
        commit('modifySkill', mutationData)
        dispatch('refreshStats')
      }
    },
    addStepSkillValues({ commit }) {
      commit('addStepSkillValues')
    },
    handleLevel({ state, commit }, data) {
      const { multiplier } = data
      if (multiplier === -1) {
        const perkStep = state.build.buildInfo.currentPerk.step
        const diff = state.build.buildInfo.currentPerk.nextAtLevel - state.build.buildInfo.currentLevel
        // console.log('handleLevel - diff => ', diff)
        let newMultiplier = 0
        if (diff) {
          newMultiplier = diff
        } else {
          newMultiplier = perkStep
        }
        // console.log('handleLevel - newMultiplier => ', newMultiplier)
        commit('handleLevel', { multiplier: newMultiplier })
      } else {
        commit('handleLevel', data)
      }
    },
    handleAvailablePerk({ state, commit }) {
      const currentLevel = state.build.buildInfo.currentLevel
      const step = state.build.buildInfo.currentPerk.step
      // console.log('handleAvailablePerk - level & step => ', currentLevel, step)
      const mutationData = {
        available: false,
        tagged: false,
        nextAtLevel: 0
      }
      let updateData = false
      if (currentLevel < step) {
        // console.log('too early')
        return
      } else if (currentLevel === step) {
        // console.log('MY FIRST PERK IS NOW AVAILABLE')
        // enable first perk
        mutationData.available = true
        mutationData.tagged = false
        mutationData.nextAtLevel = step
        updateData = true
      } else if (currentLevel % step === 0 && currentLevel < 25) {
        // console.log('MY NEXT PERK IS NOW AVAILABLE')
        // enable next perk
        mutationData.available = true
        mutationData.tagged = false
        mutationData.nextAtLevel = step
        updateData = true
      } else if (currentLevel >= 30) {
        // console.log('DISABLING PERKS')
        mutationData.available = false
        updateData = true
      }
      if (updateData) {
        // send mutation data
        // console.log('mutation data => ', mutationData)
        updateData = false
        commit('handleAvailablePerk', mutationData)
      }
    },
    toggleDrug({ dispatch, commit }, data) {
      commit('toggleDrug', data)
      dispatch('refreshStats')
    },
    changeProgressBarStyle({ commit }) {
      commit('changeProgressBarStyle')
    },
    implantSPECIAL({ dispatch, commit }, data) {
      commit('implantSPECIAL', data)
      dispatch('refreshStats')
    },
    implantCombat({ dispatch, commit }, data) {
      commit('implantCombat', data)
      dispatch('refreshStats')
    },
    takeProfession({ dispatch, commit }, data) {
      commit('takeProfession', data)
      dispatch('refreshStats')
    },
    updateBuildDetails({ commit }, data) {
      commit('updateBuildDetails', data)
    },
    equipItem({ dispatch, commit }, data) {
      commit('equipItem', data)
      dispatch('refreshStats')
    },
    equipDefaultGear({ commit }) {
      commit('equipDefaultGear')
    }
  },
  modules: {
  }
})