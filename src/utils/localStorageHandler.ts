const currentBuild = {
  static: [],
  dynamic: [],
  last: {}
}

const slot = {
  id: -1,
  slotName: '',
  inUse: false,
  char: {
    name: '',
    class: '',
    level: '',
    dateCreated: '',
    species: '',
  }
}
const prefix = 'geck_'
const slotLimit = 11

const initLocalStorage = () => {
  const slots = []
  for (let i = 0; i < slotLimit; i++) {
    localStorage.setItem(prefix + `slot${i}`, JSON.stringify({}))
    slots.push({
      ...slot,
      id: i,
      slotName: `slot${i}`,
    })
  }
  localStorage.setItem(prefix + 'slotsInfo', JSON.stringify(slots))
  localStorage.setItem(prefix + 'currentBuild', JSON.stringify(currentBuild))
  localStorage.setItem(prefix + 'globalSettings', JSON.stringify({}))
  localStorage.setItem(prefix + 'populated', 'true')
  console.log('localStorage initialized!')
}
const saveLastBuild = (build) => {
  // console.log(build)
  const currentBuild = JSON.parse(localStorage.getItem(prefix + 'currentBuild'))
  if (currentBuild) {
    currentBuild.last = build
    localStorage.setItem(prefix + 'currentBuild', JSON.stringify(currentBuild))
  }
}
const saveBuildLS = (data) => {
  const { type, slot, build } = data
  saveLastBuild(build)
  const currentBuild = JSON.parse(localStorage.getItem(prefix + 'currentBuild'))
  if (type === 'static') {
    currentBuild.static[slot] = build
  } else if (type === 'dynamic') {
    currentBuild.dynamic.push(build)
    if (currentBuild.dynamic.length > 3) {
      currentBuild.dynamic.shift()
    }
  } else {
    return
  }
  localStorage.setItem(prefix + 'currentBuild', JSON.stringify(currentBuild))
}

const getCurrentBuild = () => {
  return JSON.parse(localStorage.getItem(prefix + 'currentBuild'))
}
const getLastBuild = () => {
  const currentBuild = JSON.parse(localStorage.getItem(prefix + 'currentBuild'))
  return currentBuild && currentBuild.last && currentBuild.last.buildInfo ? currentBuild.last : false
}

const resetCurrentBuild = () => {
  localStorage.setItem(prefix + 'currentBuild', JSON.stringify(currentBuild))
}

export default {
  initLocalStorage,
  saveLastBuild,
  saveBuildLS,
  getCurrentBuild,
  getLastBuild,
  resetCurrentBuild
}